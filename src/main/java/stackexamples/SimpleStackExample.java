package stackexamples;

import java.util.Stack;

public class SimpleStackExample {
    public static void main(String[] args) {
        Stack<String> heroes = new Stack<>();
        heroes.push("superman");
        heroes.push("batman");
        heroes.push("wonder woman");
        heroes.push("aquaman");

        System.out.println(heroes.peek());
        System.out.println(heroes.pop());
        System.out.println(heroes.pop());
    }
}
