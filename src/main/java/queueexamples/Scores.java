package queueexamples;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Scores {

    public static void main(String[] args) {
        Queue<Integer> minHeap = new PriorityQueue<>(); // min-heap
        Queue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        }); // max-heap

        Scanner keyboard = new Scanner(System.in);
        while (true) {
            System.out.println("Please enter a number: ");
            int num = Integer.parseInt(keyboard.nextLine());
            minHeap.add(num);
            maxHeap.add(num);
            System.out.println("The biggest number so far is: " + maxHeap.peek());
            System.out.println("The smallest number so far is: " + minHeap.peek());
        }
    }
}
