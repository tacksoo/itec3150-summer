package serilizableexamples;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

public class City implements Serializable {
    private String name;
    private double latitude;
    private double longitude;
    private double temperature;

    // latitude and longtuide at the end of the url (separated by a comma)
    public static final String DARK_SKY = "https://api.darksky.net/forecast/3c5084c558861c1610447b49a45f4eb4/";

    public City(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * download temperatue from dark sky api
     */
    public void downloadTemperature() {
        ObjectMapper mapper = new ObjectMapper();
        String darksky = DARK_SKY + latitude + "," + longitude;
        try {
            URL url = new URL(darksky);
            String json = IOUtils.toString(url.openStream(),"UTF-8");
            JsonNode root = mapper.readTree(json);
            this.temperature = Double.parseDouble(root.get("currently").get("temperature").toString());
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", temperature=" + temperature +
                '}';
    }
}
