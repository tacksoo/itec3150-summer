package queueexamples;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class LinkedListExample {
    public static void main(String[] args) {
        Queue<String> ll = new LinkedList<>();
        ll.add("john");
        ll.add("joan");
        ll.add("aaron");
        ll.add("omar");
        System.out.println(ll);
        System.out.println(ll.peek());
        System.out.println(ll.poll());
        System.out.println(ll);
        Queue<Integer> nums = new PriorityQueue<>();
        nums.add(10);
        nums.add(15);
        nums.add(1);
        nums.add(100);
        nums.add(1000);
        nums.add(-5);
        nums.add(123);
        System.out.println(nums);
        System.out.println(nums.poll());
        System.out.println(nums.poll());
        System.out.println(nums.poll());
    }
}
