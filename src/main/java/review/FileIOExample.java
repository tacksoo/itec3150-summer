package review;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.util.List;

public class FileIOExample {
    public static final String WEATHER_URL = "https://api.darksky.net/forecast/3c5084c558861c1610447b49a45f4eb4/33.951261,-84.033882";

    public static void main(String[] args) throws Exception {
        // read from a file using FileUtils
        List<String> lines = FileUtils.readLines(new File("states.txt"),"UTF-8");
        System.out.println(lines);

        // read from a URL
        URL url = new URL(WEATHER_URL);
        // this is not the recommended way
        /*
        InputStream is = url.openStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = "";
        while((line = br.readLine()) != null ) {
            System.out.println(line);
        }
        */
        // this is the recommended way
        String json = IOUtils.toString(url.openStream(),"UTF-8");
        System.out.println(json);

        // reading a value from a JSON string
        ObjectMapper mapper = new ObjectMapper();
        JsonNode tree = mapper.readTree(json);
        JsonNode currently = tree.get("currently");
        JsonNode temperature = currently.get("temperature");
        String tempStr = temperature.toString();
        double temp = Double.parseDouble(tempStr);
        System.out.println(temp);

    }

}
