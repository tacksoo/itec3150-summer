package org.example;

import java.util.HashSet;
import java.util.Set;

public class Problem1 {

    public static void main(String[] args) {
        // given an list of ints, give me all the unique ints
        // hint: use a set!
        int[] nums = { 1,1,1,2,2,2,2,3,3,3,4,5,6,7,8,9,10 };
        Set<Integer> uniqueNums = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            uniqueNums.add(nums[i]);
        }
        System.out.println(uniqueNums);
        System.out.println("itec3150".hashCode());
        System.out.println("itec3150".hashCode());
    }

}
