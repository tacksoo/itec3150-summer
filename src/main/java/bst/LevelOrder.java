package bst;

import java.util.LinkedList;
import java.util.Queue;

public class LevelOrder {

    public static void main(String[] args) {
        Node thirtyThree = new Node(33,null,null);
        Node fortyOne = new Node(41,null,null);
        Node twentyOne = new Node(21,null,null);
        Node thirtyEight = new Node(38,thirtyThree,fortyOne);
        Node seventyTwo = new Node(72,null,null);
        Node sixty = new Node(60,null,seventyTwo);
        Node fifty = new Node(50, null,sixty);
        Node twentySeven = new Node(27, twentyOne, thirtyEight);
        Node root = new Node(42, twentySeven, fifty);
        //BSTExample.inOrder(root);
        printLevelOrder(root);
    }

    public static void printLevelOrder(Node root) {
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        while(!q.isEmpty()) {
            Node head = q.poll();
            System.out.print(head.getValue() + " ");
            if(head.getLeftChild() != null)
                q.add(head.getLeftChild());
            if(head.getRightChild() != null)
                q.add(head.getRightChild());
        }
    }
}
