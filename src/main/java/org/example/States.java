package org.example;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * States.java
 * 5/27/2021 Thursday
 * Create a map where keys are abbreviation of US states and values are state names
 * Print out size and the entry (key-value) pair for Georgia
 *
 */
public class States {

    /**
     * Main entry point to the program
     * @param args command line args
     */
    public static void main(String[] args) throws IOException {
        /* this is the hard way, is there a better way to do this?
        Scanner scanner = new Scanner(new File("states.txt"));
        while (scanner.hasNextLine()) {
            System.out.println(scanner.nextLine());
        }
        scanner.close();
         */
        List<String> lines = FileUtils.readLines(new File("states.txt"),"UTF-8");
        // for every line, split it (based on comma), and insert pair into map
        Map<String,String> states = new HashMap<>();
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] words = line.split(",");
            states.put(words[1], words[0]); // insert (k,v) where (abbrev, state name)
        }
        System.out.println(states);
        System.out.println("The size of the HashMap is " + states.size());
        System.out.println("The state name for GA is " + states.get("GA"));
        // bi-directional map (use bimap from guava library)

    }
}
