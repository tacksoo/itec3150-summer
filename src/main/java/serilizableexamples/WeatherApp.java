package serilizableexamples;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class WeatherApp {

    // instantiated a City object that represents Atlanta
    // get the current temperature
    // serialize it the Atlanta object

    public static void main(String[] args) throws IOException {
        City atlanta = new City("Atlanta",33.7679192,-84.5606918);
        atlanta.downloadTemperature();
        System.out.println(atlanta);

        File file = new File("atlanta.ser");
        byte[] data = SerializationUtils.serialize(atlanta);
        FileUtils.writeByteArrayToFile(file,data);

    }
}
