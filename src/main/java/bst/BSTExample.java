package bst;

public class BSTExample {

    public static void main(String[] args) {
        Node sixty = new Node(60,null,null);
        Node fortyFive = new Node(45, null,null);
        Node fifteen = new Node(15,null,null);
        Node forty = new Node(40,null,null);
        Node fifty = new Node(50,fortyFive,sixty);
        Node thirty = new Node(30,fifteen,forty);
        Node root = new Node(42,thirty,fifty);
        postOrder(root);
    }

    /**
     * Give you the sorted order given a bst
     * @param node
     */
    public static void inOrder(Node node) {
        if (node == null) {
            return;
        }
        inOrder(node.getLeftChild());
        System.out.println(node.getValue());
        inOrder(node.getRightChild());
    }

    public static void preOrder(Node node) {
        if (node == null) return;
        System.out.println(node.getValue());
        preOrder(node.getLeftChild());
        preOrder(node.getRightChild());
    }

    public static void postOrder(Node node) {
        if (node == null) return;
        postOrder(node.getLeftChild());
        postOrder(node.getRightChild());
        System.out.println(node.getValue());
    }




}
